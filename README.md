# HuntTheWumpus

Proyecto generado en Angular 16 como prueba técnica para puesto de trabajo Front-End Web Developer.
Se han realizado los tests unitarios en Karma, ya integrado.

## ¿Cómo ejecutarlo?
En esta guía especifica paso a paso cómo hacerlo: https://www.makeuseof.com/angular-app-clone-run/
Se debe tener nodeJS (https://nodejs.org/es) y angular-cli (https://angular.io/cli) instalado en el equipo.

Como alternativa, esta puede ser otra forma:
1.- Descargar el proyecto en .zip
2.- Descomprimir el archivo
3.- Abrir una terminal desde dentro de la carpeta descomprimida
4.- Ejecutar `npm install` en la terminal
5.- Ejecutar `ng serve --open` en la terminal, se abrirá el navegador predeterminado con el proyecto en marcha

## Previsualización
En movil: https://gitlab.com/marclopez/hunt-the-wumpus/-/blob/3c19b6f14e494ca2db49765427554cfa0db4ffdb/preview/movil.png
En tablet: https://gitlab.com/marclopez/hunt-the-wumpus/-/blob/main/preview/tablet.png
En desktop: https://gitlab.com/marclopez/hunt-the-wumpus/-/blob/main/preview/desktop.png



## Angular data

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
