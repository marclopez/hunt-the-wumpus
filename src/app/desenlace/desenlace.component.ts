import { Component } from '@angular/core';
import { LogicaJuegoService } from '../logica-juego.service';

@Component({
  selector: 'app-desenlace',
  templateUrl: './desenlace.component.html',
  styleUrls: ['./desenlace.component.css']
})
export class DesenlaceComponent {

  public desenlace:string[]=[];

  constructor(private logicaJuego: LogicaJuegoService) {
    let resultados = this.logicaJuego.getResultados();
    if(resultados['jugador']){
      this.desenlace.push(`¡Has salido con vida!`);
      if(resultados['wumpus']){
        this.desenlace.push(`El Wumpus sigue vivo un día más.`);
      }else{
        this.desenlace.push(`El Wumpus murió durante tu aventura, ¡pobre monstruo!`);
      }
      if(resultados['oro']){
        this.desenlace.push(`El oro está en tus manos. ¡VICTORIA!`);
      }else{
        this.desenlace.push(`Te has dejado el oro allí dentro. ¿Una retirada a tiempo es una victoria?`);
      }
    }else{
      this.desenlace.push(`Un final repentino... DEP`);
    }
  }

  /**
   * Recarga la web para volver a jugar.
   */
  public reiniciarJuego(){
    window.location.reload();
  }
}
