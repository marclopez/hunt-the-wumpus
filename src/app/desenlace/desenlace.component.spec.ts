import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesenlaceComponent } from './desenlace.component';
import { LogicaJuegoService } from '../logica-juego.service';

describe('DesenlaceComponent', () => {
  let component: DesenlaceComponent;
  let fixture: ComponentFixture<DesenlaceComponent>;
  let mockLogicaJuegoService: any;

  beforeEach(() => {
    mockLogicaJuegoService = jasmine.createSpyObj(['getResultados']);
    mockLogicaJuegoService.getResultados.and.returnValue({
      jugador: true,
      wumpus: false,
      oro: true,
    });
    TestBed.configureTestingModule({
      declarations: [DesenlaceComponent],
      providers: [
        { provide: LogicaJuegoService, useValue: mockLogicaJuegoService },
      ],
    });
    fixture = TestBed.createComponent(DesenlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('debe crear el componente', () => {
    expect(component).toBeTruthy();
  });

  it('un objeto con 3 keys y 3 booleans por valor son entregados por LogicaJuegoService getResultados', () => {
    const result = mockLogicaJuegoService.getResultados();
    const expected = { jugador: true, wumpus: false, oro: true };
    expect(result).toEqual(expected);
  });

  it('si jugador es true, mostrar ¡Has salido con vida!', () => {
    const result = mockLogicaJuegoService.getResultados();
    if (result.jugador) {
      component.desenlace.push('¡Has salido con vida!');
    }
    const fixture = TestBed.createComponent(DesenlaceComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('p')?.textContent).toContain(
      component.desenlace[0]
    );
  });
});
