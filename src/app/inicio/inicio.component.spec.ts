import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioComponent } from './inicio.component';
import { FormsModule } from '@angular/forms';

describe('InicioComponent', () => {
  let component: InicioComponent;
  let fixture: ComponentFixture<InicioComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [InicioComponent],
    });
    fixture = TestBed.createComponent(InicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('debe crear el componente', () => {
    expect(component).toBeTruthy();
  });

  it('si mostrarAviso es false, el div#aviso no debe mostrarse', () => {
    component.mostrarAviso = false;
    const fixture = TestBed.createComponent(InicioComponent);
    fixture.detectChanges();
    expect(
      fixture.debugElement.nativeElement.querySelector('#aviso')
    ).toBeNull();
  });
});
