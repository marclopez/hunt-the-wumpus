import { Component } from '@angular/core';
import { LogicaJuegoService } from '../logica-juego.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent {
  public x:number = 2;
  public y:number = 2;
  public pozos:number = 1;
  public flechas:number = 1;
  public mostrarAviso = false;

  constructor(private logicaJuego: LogicaJuegoService) {}

  /**
   * Valida que los valores numericos sean correctos.
   * Cuando no son correctos avisa al usuario.
   * @param e es el evento de cambio del input.
   */
  valoresModificados(e:any){
    if(this.x < 2){
      this.x = 2;
      this.mostrarAviso = true;
    }
    if(this.y < 2){
      this.y = 2;
      this.mostrarAviso = true;
    }
    if(this.pozos > (this.x * this.y - 3)){
      this.pozos = (this.x * this.y - 3);
      this.mostrarAviso = true;
    } 

    if(this.pozos < 0) this.pozos = 0;
    if(this.flechas < 0) this.flechas = 0;
    
    if(this.mostrarAviso){
      setTimeout(() => {
        this.mostrarAviso = false;
      }, 3000);
    }
  }

  /**
   * Inicializa el juego con los parametros indicados.
   */
  public lanzarJuego(){
    this.logicaJuego.prepararJuego(this.x, this.y, this.pozos, this.flechas);
  }

}
