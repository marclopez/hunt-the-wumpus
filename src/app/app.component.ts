import { Component } from '@angular/core';
import { LogicaJuegoService } from './logica-juego.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hunt The Wumpus';

  constructor(private logicaJuego: LogicaJuegoService) {}

  public getJuegoEmpezado(){
    return this.logicaJuego.getJuegoEmpezado();
  }

  public getFinDelJuego(){
    return this.logicaJuego.getFinDelJuego();
  }
}
