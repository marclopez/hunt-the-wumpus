import { TestBed } from '@angular/core/testing';

import { LogicaJuegoService } from './logica-juego.service';

describe('LogicaJuegoService', () => {
  let service: LogicaJuegoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LogicaJuegoService);
  });

  it('debe crear el servicio', () => {
    expect(service).toBeTruthy();
  });

  it('prepararJuego debe instanciar el jugador, el wumpus y el oro', () => {
    service.prepararJuego(2, 2, 0, 0);
    let statesAfterInit = service.getResultados();
    let result =
      statesAfterInit.jugador && statesAfterInit.wumpus && !statesAfterInit.oro
        ? true
        : false;
    expect(result).toBeTrue();
  });

  it('situarSalida debe situar el punto de partida dentro de los limites', () => {
    service.prepararJuego(2, 2, 0, 0);
    let result = false;
    if (
      service.getPosicionSalida()[0] >= 0 &&
      service.getPosicionSalida()[0] <= service.getDimensionesTablero()[0] &&
      service.getPosicionSalida()[1] >= 0 &&
      service.getPosicionSalida()[1] <= service.getDimensionesTablero()[1]
    ) {
      result = true;
    }
    expect(result).toBeTrue();
  });

  it('situarElementoEnTablero siempre debe posicionar el elemento en un sitio diferente al indicado', () => {
    service.prepararJuego(2, 2, 0, 0);
    let vector = service.situarElementoEnTablero([[0, 0]]);
    let result = vector[0] == 0 && vector[1] == 0 ? false : true;
    expect(result).toBeTrue();
  });

  it('is enPausa es true a causa del wumpus o un pozo tras el movimiento, el registro debe incluir jugadorDevorado o jugadorCae', () => {
    service.prepararJuego(2, 2, 0, 0);
    service.moverJugador(true);
    service.girarJugador(true);
    service.moverJugador(true);
    service.girarJugador(true);
    service.moverJugador(true);
    service.girarJugador(true);
    service.moverJugador(true);
    let registro = service.getRegistro();
    let result = false;
    if (
      ((registro.includes('jugadorDevorado') ||
        registro.includes('jugadorCae')) &&
        service.getEnPausa()) ||
      (!registro.includes('jugadorDevorado') &&
        !registro.includes('jugadorCae') &&
        !service.getEnPausa())
    ) {
      result = true;
    }
    expect(result).toBeTrue();
  });
});
