export class WumpusClass {
  private posicion: number[];
  private estaVivo: boolean;

  constructor(posicion: number[]) {
    this.estaVivo = true;
    this.posicion = posicion;
  }

  public getPosicion() {
    return this.posicion;
  }

  public getEstaVivo() {
    return this.estaVivo;
  }

  public matar() {
    this.estaVivo = false;
  }
}
