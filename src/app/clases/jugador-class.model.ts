export class JugadorClass {
  private posicion: number[];
  private flechas: number;
  private orientacion: number;
  private estaVivo: boolean;

  constructor(posicion: number[], flechas: number) {
    this.estaVivo = true;
    this.posicion = posicion;
    this.flechas = flechas;

    let posiblesOrientaciones = [0, 90, 180, 270];
    this.orientacion =
      posiblesOrientaciones[Math.floor(Math.random() * (4 - 0) + 0)];
  }

  public getPosicion() {
    return this.posicion;
  }

  public getOrientacion() {
    return this.orientacion;
  }

  public getEstaVivo() {
    return this.estaVivo;
  }

  public getFlechas(){
    return this.flechas;
  }

  /**
   * Esta funcion gira al jugador.
   * @param derecha indica si gira hacia la derecha o no mediante un boolean.
   * @returns la nueva orientación en grados
   */
  public girar(derecha: boolean) {
    let sentido = 1;
    if (!derecha) {
      sentido = sentido * -1;
    }
    // añade o resta 90 grados a la orientacion
    this.orientacion += 90 * sentido;
    // corrige en caso de que supere los 270 o baje de los 0 grados
    if (this.orientacion > 270) {
      this.orientacion = 0;
    } else if (this.orientacion < 0) {
      this.orientacion = 270;
    }
    return this.orientacion;
  }

  /**
   * Esta funcion mueve al jugador.
   * Se tiene en cuenta la orientación del jugador para considerar hacia donde avanza.
   * @param avanzar indica si va hacia adelante o hacia atrás mediante un boolean.
   * @param limites del tablero se tienen en cuenta para que no se salga de él.
   * @returns un boolean indicando si se ha podido mover o no
   */
  public mover(avanzar: boolean, limites: number[]) {
    if (
      (avanzar && this.orientacion == 0) ||
      (!avanzar && this.orientacion == 180)
    ) {
      // el jugador mira hacia el norte y quiere avanzar
      // el jugador mira hacia el sur y quiere retroceder
      // no puede sobresalir del limite Y
      if (this.posicion[1] == limites[1]) {
        return false;
      } else {
        this.posicion[1]++;
      }
    } else if (
      (avanzar && this.orientacion == 90) ||
      (!avanzar && this.orientacion == 270)
    ) {
      // el jugador mira hacia el este y quiere avanzar
      // el jugador mira hacia el oeste y quiere retroceder
      // no puede sobresalir del limite X
      if (this.posicion[0] == limites[0]) {
        return false;
      } else {
        this.posicion[0]++;
      }
    } else if (
      (avanzar && this.orientacion == 180) ||
      (!avanzar && this.orientacion == 0)
    ) {
      // el jugador mira hacia el sur y quiere avanzar
      // el jugador mira hacia el norte y quiere retroceder
      // su valor Y no puede ser inferior a 0
      if (this.posicion[1] == 0) {
        return false;
      } else {
        this.posicion[1]--;
      }
    } else if (
      (avanzar && this.orientacion == 270) ||
      (!avanzar && this.orientacion == 90)
    ) {
      // el jugador mira hacia el oeste y quiere avanzar
      // el jugador mira hacia el este y quiere retroceder
      // su valor Y no puede ser inferior a 0
      if (this.posicion[0] == 0) {
        return false;
      } else {
        this.posicion[0]--;
      }
    }
    return true;
  }

  /**
   * Esta funcion dispara una flecha.
   * @returns la cantidad de flechas restantes
   */
  public dispararFlecha() {
    if (this.flechas != 0) {
      this.flechas--;
    }
    return this.flechas;
  }

  public morir() {
    this.estaVivo = false;
  }

  /**
   * Esta funcion permite salir del tablero al jugador, si se encuentra en la posicion de salida.
   * @param posicionSalida para compararla con la del jugador.
   * @returns un boolean mostrando si tiene exito o no
   */
  public salir(posicionSalida: number[]) {
    if (
      posicionSalida[0] == this.posicion[0] &&
      posicionSalida[1] == this.posicion[1]
    ) {
      return true;
    }
    return false;
  }
}
