export class PozosClass {
    private posicion:number[];

    constructor(posicion:number[]) {
        this.posicion = posicion;
    }

    public getPosicion() {
        return this.posicion;
    }
}
