import { JugadorClass } from './jugador-class.model';

describe('JugadorClass', () => {
  it('debe crear una instancia', () => {
    expect(new JugadorClass([0, 0], 0)).toBeTruthy();
  });

  it('debe tener el metodo mover', () => {
    let jugador = new JugadorClass([0, 0], 0);
    expect(jugador.mover).toBeDefined();
  });

  it('no se puede salir de los limites al avanzar', () => {
    let jugador = new JugadorClass([0, 0], 0);
    let result: boolean = jugador.mover(true, [1, 1]);
    let expected: boolean;
    if (jugador.getOrientacion() == 270 || jugador.getOrientacion() == 180) {
      expected = false;
    } else {
      expected = true;
    }
    expect(result).toEqual(expected);
  });
});
