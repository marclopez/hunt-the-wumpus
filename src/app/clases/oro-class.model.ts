export class OroClass {
    private posicion:number[];
    private recogido:boolean;

    constructor(posicion:number[]) {
        this.posicion = posicion;
        this.recogido = false;
    }

    public getPosicion(){
        return this.posicion;
    }
    public getRecogido(){
        return this.recogido;
    }

    public recogerOro(){
        this.recogido = true;
    }
}
