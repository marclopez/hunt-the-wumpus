import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JuegoComponent } from './juego.component';
import { RegistroComponent } from './UI/registro/registro.component';
import { ControlesComponent } from './UI/controles/controles.component';

describe('JuegoComponent', () => {
  let component: JuegoComponent;
  let fixture: ComponentFixture<JuegoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JuegoComponent, RegistroComponent, ControlesComponent],
    });
    fixture = TestBed.createComponent(JuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('debe crear el componente', () => {
    expect(component).toBeTruthy();
  });
});
