import { Component } from '@angular/core';
import { LogicaJuegoService } from 'src/app/logica-juego.service';

@Component({
  selector: 'app-controles',
  templateUrl: './controles.component.html',
  styleUrls: ['./controles.component.css']
})
export class ControlesComponent {

  constructor(private logicaJuego: LogicaJuegoService) {}

  public avanzar(){
    if(!this.logicaJuego.getEnPausa()) this.logicaJuego.moverJugador(true);
  }
  public retroceder(){
    if(!this.logicaJuego.getEnPausa()) this.logicaJuego.moverJugador(false);
  }
  public girarIzquierda(){
    if(!this.logicaJuego.getEnPausa()) this.logicaJuego.girarJugador(false);
  }
  public girarDerecha(){
    if(!this.logicaJuego.getEnPausa()) this.logicaJuego.girarJugador(true);
  }
  public salir(){
    if(!this.logicaJuego.getEnPausa()) this.logicaJuego.salir();
  }
  public lanzarFlecha(){
    if(!this.logicaJuego.getEnPausa()) this.logicaJuego.dispararFlecha();
  }
}
