import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { LogicaJuegoService } from 'src/app/logica-juego.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {

  public registro:string[];
  private informacionParaElJugador = new Map<string, string>();
  

  constructor(private logicaJuego: LogicaJuegoService) {
    this.registro = logicaJuego.getRegistro();
    this.informacionParaElJugador.set('jugadorMovido',`Te has movido.`);
    this.informacionParaElJugador.set('jugadorDevorado',`El Wumpus te ha devorado! Fin del juego.`);
    this.informacionParaElJugador.set('wumpusCadaver',`Has encontrado el cadáver del Wumpus, una imagen que te perseguirá en sueños.`);
    this.informacionParaElJugador.set('jugadorCae',`Un paso en falso, caes al vacío en un enorme pozo. Fin del juego.`);
    this.informacionParaElJugador.set('oro',`Algo brilla a tus pies, lo recoges: ¡es el oro! Ahora solo tienes que salir de aquí con vida.`);
    this.informacionParaElJugador.set('salida',`Hay luz en esta grieta, por aquí puedes salir.`);
    this.informacionParaElJugador.set('jugadorChoca',`Chocas contra una pared.`);
    this.informacionParaElJugador.set('wumpusCerca',`Percibes un fuerte hedor, el Wumpus está cerca.`);
    this.informacionParaElJugador.set('pozoCerca',`Una brisa te acaricia, hay un pozo cerca.`);
    this.informacionParaElJugador.set('wumpusMuerto',`Un terrible chillido de dolor resuena, ¡has matado al Wumpus con tu flecha!`);
    this.informacionParaElJugador.set('flechaDisparada',`Lanzas una flecha.`);
    this.informacionParaElJugador.set('sinFlechas',`No te quedan flechas.`);
    this.informacionParaElJugador.set('jugadorSale',`¡Sales de aquí! Fin del juego.`);
    this.informacionParaElJugador.set('jugadorNoSale',`No hay ninguna salida en esta casilla.`);
    this.informacionParaElJugador.set('jugadorGira90',`Te giras, ahora miras al este.`);
    this.informacionParaElJugador.set('jugadorGira180',`Te giras, ahora miras al sur.`);
    this.informacionParaElJugador.set('jugadorGira270',`Te giras, ahora miras al oeste.`);
    this.informacionParaElJugador.set('jugadorGira0',`Te giras, ahora miras al norte.`);
    this.informacionParaElJugador.set('bienvenido',`Utiliza la cruceta para (↑)avanzar, (↓)retroceder o (← →)girar. Con el botón (A) podrás lanzar una flecha y con el (B) salir, siempre y cuando estés en la misma casilla de origen. ¿Preparado?`);    
  }

  public getInformacionParaElJugador(key:string){
    return this.informacionParaElJugador.get(key);
  }

  /* pequeña funcionalidad para manter el registro deslizado hacia abajo, como un chat
  https://stackblitz.com/edit/angular-auto-scroll-ns2p9w?file=src%2Fapp%2Fapp.component.css,src%2Fapp%2Fapp.component.ts */
  @ViewChild('scrollframe', {static: false}) scrollFrame!: ElementRef;
  @ViewChildren('item') itemElements!: QueryList<any>;
  
  private scrollContainer: any;

  ngAfterViewInit() {
    this.scrollContainer = this.scrollFrame.nativeElement;
    this.itemElements.changes.subscribe(_ => this.onItemElementsChanged());    
  }
  
  private onItemElementsChanged(): void {
    this.scrollToBottom();
  }

  private scrollToBottom(): void {
    this.scrollContainer.scroll({
      top: this.scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }

}
