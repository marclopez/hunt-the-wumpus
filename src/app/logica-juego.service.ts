import { Injectable } from '@angular/core';
import { JugadorClass } from './clases/jugador-class.model';
import { WumpusClass } from './clases/wumpus-class.model';
import { OroClass } from './clases/oro-class.model';
import { PozosClass } from './clases/pozos-class.model';

@Injectable({
  providedIn: 'root',
})
export class LogicaJuegoService {
  // estados del juego
  private juegoEmpezado: boolean = false;
  private finDelJuego: boolean = false;
  private enPausa: boolean = false;
  private registro: string[] = [];

  // parametros del juego
  private dimensionesTablero: number[] = [0, 0];
  private pozos: PozosClass[] = [];
  private posicionSalida: number[] = [0, 0];
  private jugador!: JugadorClass;
  private wumpus!: WumpusClass;
  private oro!: OroClass;

  constructor() {}

  public getRegistro() {
    return this.registro;
  }
  public getJuegoEmpezado() {
    return this.juegoEmpezado;
  }
  public getFinDelJuego() {
    return this.finDelJuego;
  }
  public getEnPausa() {
    return this.enPausa;
  }
  public getOrientacionJugador() {
    return this.jugador.getOrientacion();
  }
  public getPosicionSalida() {
    return this.posicionSalida;
  }
  public getDimensionesTablero() {
    return this.dimensionesTablero;
  }
  public getResultados() {
    return {
      jugador: this.jugador.getEstaVivo(),
      wumpus: this.wumpus.getEstaVivo(),
      oro: this.oro.getRecogido(),
    };
  }

  /**
   * Esta funcion prepara el tablero de juego, asignando posiciones aleatorias a los diferentes elementos.
   * @param ladoX el limite de casillas en el lado X.
   * @param ladoY el limite de casillas en el lado Y.
   * @param pozos cantidad de pozos en el tablero.
   * @param flechas cantidad de flechas disponibles.
   */
  public prepararJuego(
    ladoX: number,
    ladoY: number,
    pozos: number,
    flechas: number
  ) {
    let posicionesOcupadas: any[] = [];

    this.dimensionesTablero[0] = ladoX - 1;
    this.dimensionesTablero[1] = ladoY - 1;

    // situar la salida
    posicionesOcupadas.push(this.situarSalida());

    // genera el jugador
    this.jugador = new JugadorClass(
      // copia en profundidad para evitar referencias
      JSON.parse(JSON.stringify(this.posicionSalida)),
      flechas
    );

    // genera el wumpus y lo posiciona aleatoriamente
    this.wumpus = new WumpusClass(
      this.situarElementoEnTablero(posicionesOcupadas)
    );
    posicionesOcupadas.push(this.wumpus.getPosicion());

    // genera el oro y lo posiciona aleatoriamente
    this.oro = new OroClass(this.situarElementoEnTablero(posicionesOcupadas));
    posicionesOcupadas.push(this.oro.getPosicion());

    // generar pozos y los posiciona aleatoriamente
    for (let i = 0; i < pozos; i++) {
      let posicion = this.situarElementoEnTablero(posicionesOcupadas);
      this.pozos.push(new PozosClass(posicion));
      posicionesOcupadas.push(posicion);
    }
    this.juegoEmpezado = true;
    this.registro.push('bienvenido');
    this.comprobarProximidades();
  }

  /**
   * Obten un valor aleatorio entre 0 y n
   * @param n el valor maximo.
   * @returns el numero aleatorio
   */
  private getNumeroAleatorio(n: number) {
    let min = Math.ceil(0);
    let max = Math.floor(n);
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  /**
   * Esta funcion posiciona la salida en el tablero de forma aleatoria.
   * La salida solo puede estar en los lados de la matriz.
   * Por lo que en caso de que X sea 0 o el valor maximo de X, Y puede estar comprendido entre 0 y el valor maximo de Y;
   * en caso contrario, cuando X sea un valor entre 0 y el maximo de X, Y solo puede ser 0 o el valor maximo de Y.
   * @returns la posicion de la salida.
   */
  private situarSalida() {
    let posicionX: number = this.getNumeroAleatorio(this.dimensionesTablero[0]);
    let posicionY: number;
    if (posicionX == 0 || posicionX == this.dimensionesTablero[0]) {
      posicionY = this.getNumeroAleatorio(this.dimensionesTablero[1]);
    } else {
      // expresion para generar un boolean aleatorio
      if (Math.random() >= 0.5) {
        posicionY = 0;
      } else {
        posicionY = this.dimensionesTablero[1];
      }
    }
    this.posicionSalida = [posicionX, posicionY];
    return this.posicionSalida;
  }

  /**
   * Posiciona un elemento en el tablero evitando las casillas previamente ocupadas
   * @param posicionesOcupadas un array con las posiciones que ya estan ocupadas.
   * @returns la posicion del nuevo elemento.
   */
  public situarElementoEnTablero(posicionesOcupadas: any[]) {
    var posicion: number[] = [];
    let repetido: boolean;
    do {
      posicion[0] = this.getNumeroAleatorio(this.dimensionesTablero[0]);
      posicion[1] = this.getNumeroAleatorio(this.dimensionesTablero[1]);
      repetido = false;
      for (let i = 0; i < posicionesOcupadas.length; i++) {
        if (
          posicionesOcupadas[i][0] == posicion[0] &&
          posicionesOcupadas[i][1] == posicion[1]
        ) {
          repetido = true;
          break;
        }
      }
    } while (repetido);
    return posicion;
  }

  // funciones de control del jugador
  /**
   * Avanza o retrocede el jugador. Si se ha podido mover, se detectan colisiones con otros actores.
   * En caso de colision, se resuelve. En caso contrario, se revisa casillas adyacentes y se resuelve en caso de encontrar algo.
   * @param avanzar true en caso de avanzar, false en caso de retroceder.
   */
  public moverJugador(avanzar: boolean) {
    // el jugador puede avanzar?
    if (this.jugador.mover(avanzar, this.dimensionesTablero)) {
      this.registro.push('jugadorMovido');
      // el jugador colisiona con otro actor?
      let resultadoColision = this.jugadorColisiona();
      if (resultadoColision) {
        // que es?
        if (resultadoColision == 'wumpus') {
          // esta vivo?
          if (this.wumpus.getEstaVivo()) {
            // el wumpus se come al jugador, fin del juego
            this.registro.push('jugadorDevorado');
            this.jugador.morir();
            this.enPausa = true;
            setTimeout(() => {
              this.finDelJuego = true;
            }, 3000);
            return;
          } else {
            // se encuentra el cadaver del wumpus
            this.registro.push('wumpusCadaver');
          }
        } else if (resultadoColision == 'pozo') {
          // el jugador cae en el pozo
          this.registro.push('jugadorCae');
          this.jugador.morir();
          this.enPausa = true;
          setTimeout(() => {
            this.finDelJuego = true;
          }, 3000);
          return;
        } else if (resultadoColision == 'oro' && !this.oro.getRecogido()) {
          // el jugador encuentra y recoge el oro
          this.registro.push('oro');
          this.oro.recogerOro();
        } else if (resultadoColision == 'salida') {
          // casilla de salida
          this.registro.push('salida');
        }
      }
      // hay algo cerca?
      this.comprobarProximidades();
    } else {
      this.registro.push('jugadorChoca');
    }
  }

  /**
   * Gira el jugador
   * @param derecha true en caso de girar a la derecha, false en caso de girar a la izquierda.
   */
  public girarJugador(derecha: boolean) {
    this.registro.push(`jugadorGira${this.jugador.girar(derecha)}`);
  }

  // interacciones con el entorno
  /**
   * Comprueba si el jugador colisiona en la misma celda con otro actor.
   * @returns false en caso de que no colisione o el tipo de actor con el que si.
   */
  private jugadorColisiona() {
    let posicionJugador = this.jugador.getPosicion();
    let posicionWumpus = this.wumpus.getPosicion();
    if (
      posicionJugador[0] == posicionWumpus[0] &&
      posicionJugador[1] == posicionWumpus[1]
    ) {
      return 'wumpus';
    }
    for (let pozo of this.pozos) {
      let posicionPozo = pozo.getPosicion();
      if (
        posicionJugador[0] == posicionPozo[0] &&
        posicionJugador[1] == posicionPozo[1]
      ) {
        return 'pozo';
      }
    }
    let posicionOro = this.oro.getPosicion();
    if (
      posicionJugador[0] == posicionOro[0] &&
      posicionJugador[1] == posicionOro[1]
    ) {
      return 'oro';
    }
    if (
      posicionJugador[0] == this.posicionSalida[0] &&
      posicionJugador[1] == this.posicionSalida[1]
    ) {
      return 'salida';
    }
    return false;
  }

  /**
   * ¿Hay algo en las casillas adyacentes?
   * @returns un array con todo lo que hay cerca, vacio en caso de que no encuentre nada
   */
  private comprobarProximidades() {
    let posicionJugador = this.jugador.getPosicion();
    let posicionWumpus = this.wumpus.getPosicion();
    if (
      (posicionJugador[0] == posicionWumpus[0] &&
        posicionJugador[1] + 1 == posicionWumpus[1]) ||
      (posicionJugador[0] + 1 == posicionWumpus[0] &&
        posicionJugador[1] == posicionWumpus[1]) ||
      (posicionJugador[0] == posicionWumpus[0] &&
        posicionJugador[1] - 1 == posicionWumpus[1]) ||
      (posicionJugador[0] - 1 == posicionWumpus[0] &&
        posicionJugador[1] == posicionWumpus[1])
    ) {
      this.registro.push('wumpusCerca');
    }
    for (let pozo of this.pozos) {
      let posicionPozo = pozo.getPosicion();
      if (
        (posicionJugador[0] == posicionPozo[0] &&
          posicionJugador[1] + 1 == posicionPozo[1]) ||
        (posicionJugador[0] + 1 == posicionPozo[0] &&
          posicionJugador[1] == posicionPozo[1]) ||
        (posicionJugador[0] == posicionPozo[0] &&
          posicionJugador[1] - 1 == posicionPozo[1]) ||
        (posicionJugador[0] - 1 == posicionPozo[0] &&
          posicionJugador[1] == posicionPozo[1])
      ) {
        this.registro.push('pozoCerca');
        break;
      }
    }
    return;
  }

  /**
   * El jugador intenta disparar una flecha y matar el Wumpus.
   */
  public dispararFlecha() {
    if (this.jugador.getFlechas() > 0) {
      // resta una flecha del carcaj del jugador
      this.jugador.dispararFlecha();
      if (this.wumpus.getEstaVivo()) {
        // en que direccion sale la flecha?
        let orientacion = this.jugador.getOrientacion();
        // donde estan los actores?
        let posicionJugador = this.jugador.getPosicion();
        let posicionWumpus = this.wumpus.getPosicion();
        if (
          (orientacion == 0 || orientacion == 180) &&
          posicionJugador[0] == posicionWumpus[0]
        ) {
          // el jugador mira al norte o sur, esta en la misma X?
          if (
            (orientacion == 0 && posicionJugador[1] < posicionWumpus[1]) ||
            (orientacion == 180 && posicionJugador[1] > posicionWumpus[1])
          ) {
            // el wumpus esta en la misma direccion en la que mira el jugador
            this.wumpus.matar();
            this.registro.push('wumpusMuerto');
            return;
          }
        } else if (
          (orientacion == 90 || orientacion == 270) &&
          posicionJugador[1] == posicionWumpus[1]
        ) {
          // el jugador mira al este o oeste, esta en la misma Y?
          if (
            (orientacion == 90 && posicionJugador[0] < posicionWumpus[0]) ||
            (orientacion == 270 && posicionJugador[0] > posicionWumpus[0])
          ) {
            // el wumpus esta en la misma direccion en la que mira el jugador
            this.wumpus.matar();
            this.registro.push('wumpusMuerto');
            return;
          }
        }
      }
      this.registro.push('flechaDisparada');
    } else {
      this.registro.push('sinFlechas');
    }
  }

  /**
   * El jugador intenta salir del tablero.
   */
  public salir() {
    if (this.jugador.salir(this.posicionSalida)) {
      this.registro.push('jugadorSale');
      this.enPausa = true;
      setTimeout(() => {
        this.finDelJuego = true;
      }, 3000);
    } else {
      this.registro.push('jugadorNoSale');
    }
  }
}
